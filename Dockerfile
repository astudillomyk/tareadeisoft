# Utiliza una imagen base de Java
FROM openjdk:20

# Establece el directorio de trabajo en /app
WORKDIR /app

# Copia el archivo pom.xml al directorio de trabajo
COPY pom.xml .

# Descarga todas las dependencias de Maven (solo el archivo pom.xml) para aprovechar el caché de Docker
RUN mvn dependency:go-offline -B

# Copia todo el código fuente al directorio de trabajo
COPY src ./src

# Compila la aplicación con Maven
RUN mvn package

# El comando para ejecutar la aplicación
CMD ["java", "-jar", "target/TareaIsoft-2023-1.0-SNAPSHOT.jar"]
