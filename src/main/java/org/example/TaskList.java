package org.example;

import org.example.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskList {
    private List<Task> tasks;

    public TaskList() {
        tasks = new ArrayList<>();
    }

    public void addTask(Task task) {
        tasks.add(task);
    }

    public void removeTaskByTitle(String title) {
        Task taskToRemove = null;
        for (Task task : tasks) {
            if (task.getTitle().equalsIgnoreCase(title)) {
                taskToRemove = task;
                break;
            }
        }
        if (taskToRemove != null) {
            tasks.remove(taskToRemove);
            System.out.println("Tarea eliminada: " + title);
        } else {
            System.out.println("La tarea con el título '" + title + "' no se encontró.");
        }
    }

    public void removeTask(int index){
        Task taskToRemove = null;
        tasks.remove(index);
    }

    public void displayTasks() {
        System.out.println("List of tasks:");
        int index = 0;
        for (Task task : tasks) {
            index++;
            System.out.println(index + ". ");
            System.out.println(task.getTitle());
            System.out.println(task.getDescription());
            if (task.isCompleted()){
                System.out.println("Task completed!\n");
            } else {
                System.out.println("Task not completed.\n");
            }
        }
    }

    public void markTaskAsCompletedByTitle(String title) {
        for (Task task : tasks) {
            if (task.getTitle().equalsIgnoreCase(title)) {
                task.markAsCompleted();
                System.out.println("Tarea marcada como completada: " + title);
                return;
            }
        }
        System.out.println("La tarea con el título '" + title + "' no se encontró.");
    }
}
