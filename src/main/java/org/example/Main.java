package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        TaskList taskList = new TaskList();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Selecciona una opción:");
            System.out.println("1. Agregar tarea");
            System.out.println("2. Eliminar tarea");
            System.out.println("3. Mostrar tareas");
            System.out.println("4. Marcar tarea como completada");
            System.out.println("5. Salir");

            int opcion = scanner.nextInt();
            scanner.nextLine(); // Limpiar el salto de línea

            switch (opcion) {
                case 1:
                    System.out.println("Ingrese el título de la tarea:");
                    String title = scanner.nextLine();
                    System.out.println("Ingrese la descripción de la tarea:");
                    String description = scanner.nextLine();
                    Task newTask = new Task(title, description);
                    taskList.addTask(newTask);
                    break;
                case 2:
                    System.out.println("Ingrese el número de la tarea que desea eliminar:");
                    int indexToRemove = scanner.nextInt();
                    indexToRemove--;
                    taskList.removeTask(indexToRemove);
                    break;
                case 3:
                    taskList.displayTasks();
                    break;
                case 4:
                    System.out.println("Ingrese el título de la tarea a marcar como completada:");
                    String titleToMark = scanner.nextLine();
                    taskList.markTaskAsCompletedByTitle(titleToMark);
                    break;
                case 5:
                    System.out.println("Saliendo del programa.");
                    scanner.close();
                    System.exit(0);
                default:
                    System.out.println("Opción no válida. Por favor, selecciona una opción válida.");
            }
        }
    }
}

